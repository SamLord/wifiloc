﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using WifiLoc.GMaps.Data;
using WifiLoc.Wireless;

namespace WifiLoc.GMaps
{
    public class ApiCommunicator
    {
        public Response GetLocationFromAccessPoints(List<WifiAccessPoint> wifiPoints)
        {
            Response returnedLocation;

            string jsonFormatted = "{\"wifiAccessPoints\":[";

            JavaScriptSerializer ser = new JavaScriptSerializer();

            for (int count = 0; count < wifiPoints.Count; count++)
            {
                jsonFormatted += ser.Serialize(wifiPoints[count]);
                if (count != wifiPoints.Count - 1)
                {
                    jsonFormatted += ",";
                }
            }
            foreach (WifiAccessPoint accessPoint in wifiPoints)
            {
            }

            jsonFormatted += "]}";

            Request req = new Request(jsonFormatted, "application/json", "POST");

            string url = "https://www.googleapis.com/geolocation/v1/geolocate?key="
                + Environment.GetEnvironmentVariable("GMapsAPIKey");

            string returnedJson = SendRequest(req, url);

            if (returnedJson == null)
            {
                throw new NullReferenceException();
            }

            Rootobject rootObj = ser.Deserialize<Rootobject>(returnedJson);

            returnedLocation = new Response(rootObj);

            return returnedLocation;
        }


        public string SendRequest(Request req, string url)
        {
            HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(url);
            webReq.ContentType = req.ContentType;
            webReq.Method = req.Method;

            string response;

            try
            {
                using (StreamWriter streamW = new StreamWriter(webReq.GetRequestStream()))
                {
                    streamW.Write(req.Json);
                }

                HttpWebResponse webResp = (HttpWebResponse)webReq.GetResponse();

                using (StreamReader streamR = new StreamReader(webResp.GetResponseStream()))
                {
                    response = streamR.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                using (StreamReader streamR = new StreamReader(ex.Response.GetResponseStream()))
                {
                    response = streamR.ReadToEnd();
                    MessageBox.Show("Error in request, response was: " + response);
                    response = null;
                }
            }

            return response;
        }

    }
}
