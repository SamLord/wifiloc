﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WifiLoc.GMaps
{
    public class ErrorResponse : Response
    {
        public string Response { get; set; }

        public ErrorResponse(string error)
        {
            Response = error;
        }
    }
}
