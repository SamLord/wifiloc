﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WifiLoc.GMaps
{
    public class Request
    {
        string jsonString;
        string contentType;
        string method;

        public Request(string json, string contentType, string method)
        {
            this.jsonString = json;
            this.contentType = contentType;
            this.method = method;
        }

        public string Json
        {
            get
            {
                return jsonString;
            }
        }

        public string ContentType
        {
            get
            {
                return contentType;
            }
        }

        public string Method
        {
            get
            {
                return method;
            }
        }
    }
}
