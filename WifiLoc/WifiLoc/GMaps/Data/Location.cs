﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WifiLoc.GMaps.Data
{

    public class Location
    {
        public float lat { get; set; }
        public float lng { get; set; }
    }
}
