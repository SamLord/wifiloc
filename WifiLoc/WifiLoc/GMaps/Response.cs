﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WifiLoc.GMaps.Data;

namespace WifiLoc.GMaps
{
    public class Response
    {
        public Location location { get; set; }
        public float accuracy { get; set; }

        public Response()
        {
        }

        public Response(Rootobject rootObj)
        {
            location = rootObj.location;
            accuracy = rootObj.accuracy;
        }

        public Response(Location lctn, float accuracy)
        {
            location = lctn;
            this.accuracy = accuracy;
        }

        #region GettersAndSetters

        public Location Location
        {
            get
            {
                return location;
            }
        }

        public float Accuracy
        {
            get
            {
                return accuracy;
            }
        }
        #endregion

    }
}
