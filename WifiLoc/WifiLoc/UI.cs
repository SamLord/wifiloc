﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WifiLoc.GMaps;
using WifiLoc.Wireless;
using NativeWifi;
using System.Threading;

namespace WifiLoc
{
    public partial class UI : Form
    {
        List<WifiAccessPoint> accessPts;

        public UI()
        {
            InitializeComponent();
        }

        private void RefreshLocation()
        {
            UpdateNearbyWireless();
            ApiCommunicator apiComm = new ApiCommunicator();

            Response resp = apiComm.GetLocationFromAccessPoints(accessPts);

            if (resp.GetType() == typeof(ErrorResponse))
            {
                MessageBox.Show("Request error: " + ((ErrorResponse)resp).Response);
            }
            else
            {
                string url = "https://www.google.com/maps/embed/v1/search?key=" + Environment.GetEnvironmentVariable("GMapsAPIKey") + "&q=" + resp.location.lat.ToString()
                    + "+" + resp.location.lng.ToString();
                webBrowser.DocumentText = "<html><head/><body><iframe src=\"" + url + "\" width=\"" + (webBrowser.Width-50).ToString() + "\" + height=\""
                    + (webBrowser.Height-50).ToString() + "\" /> </body></html>";
            }
        }

        private void refreshButton_Click(object sender, EventArgs e)
        {
            Thread refreshThread = new Thread(new ThreadStart(RefreshLocation));
            refreshThread.Start();
        }

        private void webBrowser_Resize(object sender, EventArgs e)
        {
            //RefreshLocation();
        }

        private void displayNetworksButton_Click(object sender, EventArgs e)
        {
            if (accessPts == null)
            {
                UpdateNearbyWireless();
            }
            wirelessListView.Items.Clear();
            foreach (WifiAccessPoint accessPt in accessPts)
            {
                ListViewItem item = new ListViewItem(accessPt.ssid);
                wirelessListView.Items.Add(item);
            }
        }

        private void UpdateNearbyWireless()
        {
            accessPts = NearbyWirelessManager.GetNearbyWireless();
        }
    }
}
