﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WifiLoc.Wireless
{
    public class WifiAccessPoint
    {
        public string macAddress { get; set; }
        public uint signalStrength { get; set; }
        public float age { get; set; }
        public string ssid { get; set; }

        public WifiAccessPoint(string mac, uint sigStrength, float age, string ssid)
        {
            macAddress = mac;
            signalStrength = sigStrength;
            this.age = age;
            this.ssid = ssid;
        }
    }
}
