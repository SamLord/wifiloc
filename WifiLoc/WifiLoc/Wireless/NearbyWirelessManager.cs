﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NativeWifi;

namespace WifiLoc.Wireless
{
    public static class NearbyWirelessManager
    {
        public static  List<WifiAccessPoint> GetNearbyWireless()
        {
            List<WifiAccessPoint> accessPts = new List<WifiAccessPoint>();
            WlanClient client = new WlanClient();

            WifiAccessPoint tempWifi;
            foreach (WlanClient.WlanInterface wlanInterface in client.Interfaces)
            {
                Wlan.WlanBssEntry[] wlanBssEntries = wlanInterface.GetNetworkBssList();
                foreach (Wlan.WlanBssEntry entry in wlanBssEntries)
                {
                    byte[] macAddress = entry.dot11Bssid;

                    int addrLen = macAddress.Length;
                    string mac = "";

                    for (int count = 0; count < addrLen; count++)
                    {
                        mac += macAddress[count].ToString("x2").PadLeft(2, '0').ToUpper(); ;
                        if (count + 1 % 2 != 0 && count != addrLen - 1)
                        {
                            mac += ":";
                        }
                    }

                    string ssid = System.Text.Encoding.UTF8.GetString(entry.dot11Ssid.SSID);
                    tempWifi = new WifiAccessPoint(mac, entry.linkQuality, 0, ssid);
                    accessPts.Add(tempWifi);
                }
            }

            return accessPts;
        }
    }
}
