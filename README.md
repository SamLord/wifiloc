# README #

### What is this repository for? ###

* Simple implementation of the google geolocation and maps api to find computer location from nearby wifi hotspots

### Upcoming ###

* Local wifi data so no internet connection is required to find lat/long

* Local/Cached map data - [Open Street Maps](http://wiki.openstreetmap.org/wiki/Downloading_data) proabaly

### How do I get set up? ###

* C#
* Visual Studio (2012/2013/2010 should all work)
* Google api key - https://console.developers.google.com/

### Licence ###

* You're not allowed to use this code unless you really want to and you accept that if things go wrong (or right) it's your fault. Although, if it goes right I wouldn't mind a mention.

### Who do I talk to? ###

* Open an issue on the issue tracker